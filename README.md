# ChatHistoryViewer

ChatHistoryViewer (CHV)

CHV Solution contains: 
	* ChatHistoryViewer - Console Application Project 
	* ChatHistoryViewer.Tests - Unit Test Project

Solution is using EF with connection to Postgresql DB, so you would need to be sure that have PostrgreSQL installed, then create PowerDiaryTestDB. 

Prerequisites and steps:
1. There should be postgresql user with password anton, this user need to have full rights on PowerDiaryTestDB.
Otherwise you can change connection string in ApplicationContext.cs file with appropriate credentials:
"Host=localhost;Port=5432;Database=PowerDiaryTestDB;Username=postgres;Password=anton"

2. In Package Manager Console you would need to run the following, it will make DB actual with Solution model:
Update-Database

3. Run ChatHistoryViewer
   Please follow the instructions in Console.
   Both for ChatHistoryViewer and ChatHistoryViewer.Tests - prepared test data is specified in PrepareTestData() method

![image.png](./image.png)

4. You can also run Unit Test in ChatHistoryTests class

![image-1.png](./image-1.png)




