﻿using System;
using System.Runtime.Serialization;

namespace ChatHistoryViewer.Helper
{
    public static class Converter
    {
        private const string DateTimeFormat = "yyyy-MM-dd HH:mm tt";

        public static DateTime StringToDateTime(string iString, string dtFormat = DateTimeFormat)
        {
            return DateTime.ParseExact(iString, DateTimeFormat, null);
        }
    }
}
