﻿using System;
using System.Collections.Generic;
using System.Reflection.Metadata.Ecma335;
using System.Text;
using ChatHistoryViewer.Enums;

namespace ChatHistoryViewer.Helper
{
    public static class MessageHelper
    {
        public static string EnterTheRoom(string name) => $"{name} enters the room";
        public static string LeaveTheRoom(string name) => $"{name} leave the room";
        public static string HighFives(string nameFrom, string nameTo) => $"{nameFrom} high-fives {nameTo}";
        public static string LeftComments(string name, string comment) => $"{name} comments: \"{comment}\"";

        public static string EnterTheRoom(int personsNumber) => $"{personsNumber} {(personsNumber < 2 ? "person" : "people")} entered";
        public static string LeaveTheRoom(int personsNumber) => $"{personsNumber} left";
        public static string HighFives(int personsNumberSent, int personsNumberReceived) => $"{personsNumberSent} {(personsNumberSent < 2 ? "person" : "people")} high-fives {(personsNumberReceived < 2 ? "person" : "people")}";
        public static string LeftComments(int commentsNumber) => $"{commentsNumber} comments";

        public static string AggregatedMessageByEventType(EventTypeEnum eventType, string nameFrom, string nameTo = null)
        {

            return null;
        }
    }
}
