﻿using System;
using System.Runtime.Serialization;

namespace ChatHistoryViewer.Helper
{
    public static class Validator
    {
        public static DateTime DateTimeValidator(string inputValue)
        {
            if (DateTime.TryParse(inputValue ?? string.Empty, out var dateTime))
                Console.WriteLine("Entered DateTime is valid");
            else
                Console.WriteLine("Unable to parse entered DateTime");

            return dateTime;
        }
    }
}
