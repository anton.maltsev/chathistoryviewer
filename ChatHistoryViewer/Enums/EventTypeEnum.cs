﻿namespace ChatHistoryViewer.Enums
{
        public enum EventTypeEnum
        {
            EnterTheRoom = 0,
            LeaveTheRoom = 1,
            Comment = 2,
            HighFiveAnotherUser = 3
        }

        public enum GranularityEnum
        {
            MinuteByMinute = 0,
            Hourly = 1
        }
}
