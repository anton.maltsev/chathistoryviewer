﻿using System;
using System.ComponentModel.DataAnnotations;
using ChatHistoryViewer.Enums;

namespace ChatHistoryViewer.Entities
{
    public class Event
    {
        public int Id { get; set; }
        public int UserId { get; set; }
        public int TargetUserId { get; set; }
        public DateTime EventDate { get; set; }
        public EventTypeEnum EventType { get; set; }
        [StringLength(255)]
        public string Value { get; set; }
    }
}
