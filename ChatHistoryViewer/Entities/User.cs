﻿using System.ComponentModel.DataAnnotations;

namespace ChatHistoryViewer.Entities
{
    public class User
    {
        public int Id { get; set; }
        
        [StringLength(50)]
        public string Name { get; set; }
        
        [StringLength(50)]
        public string Surname { get; set; }
    }
}
