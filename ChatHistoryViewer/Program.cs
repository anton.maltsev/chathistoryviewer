﻿using System;
using System.Collections.Generic;
using System.Linq;
using ChatHistoryViewer.Context;
using ChatHistoryViewer.Entities;
using ChatHistoryViewer.Enums;
using ChatHistoryViewer.Helper;
using ChatHistoryViewer.Repositories;

namespace ChatHistoryViewer
{
    public class Program
    {
        private static DateTime _startDateTime;
        private static DateTime _endDateTime;

        static void Main()
        {
            Console.Write("Chat History Viewer\n");

            #region Prepare Data Block

            var eventsRepo = new EventRepository();
            eventsRepo.DeleteEvents();

            var userRepo = new UserRepository();
            userRepo.DeleteUsers();

            PrepareTestData();

            #endregion

            while (Console.ReadKey().Key != ConsoleKey.X)
            {
                Console.WriteLine("\nDo you want to get detailed or aggregated events?(D/A)");
                var keyInfo = Console.ReadKey();
                
                switch (keyInfo.Key)
                {
                    case ConsoleKey.D:
                        break;
                    case ConsoleKey.A:
                        break;
                    default:
                        continue;
                }

                Console.WriteLine("\nEnter Start DateTime"); //in the following format: yyyy/MM/dd HH:mm

                _startDateTime = Validator.DateTimeValidator(Console.ReadLine());

                Console.WriteLine("Enter End DateTime in the following format: MM/dd/yyyy");

                _endDateTime = Validator.DateTimeValidator(Console.ReadLine());

                object eventsList = keyInfo.Key switch
                {
                    ConsoleKey.D => eventsRepo.GetDetailedEventsHistory(_startDateTime, _endDateTime),
                    ConsoleKey.A => eventsRepo.GetAggregatedEventsHistory(_startDateTime, _endDateTime),
                    _ => null
                };

                if (eventsList != null)
                {
                    var eventsListType = eventsList.GetType();

                    switch (eventsListType)
                    {
                        case { } when eventsListType == typeof(Dictionary<string, string>):

                            foreach (var (key, value) in (Dictionary<string, string>) eventsList)
                            {
                                Console.WriteLine($"{key} {value}");
                            }

                            break;
                        case { } when eventsListType == typeof(List<Tuple<string, string>>):

                            foreach (var item in (List<Tuple<string, string>>)eventsList)
                            {
                                Console.WriteLine($"{item.Item1} {item.Item2}");
                            }

                            break;
                    }
                }

                Console.WriteLine("\nDo you want to continue? (Y/X)");
                keyInfo = Console.ReadKey();

                if (keyInfo.Key == ConsoleKey.X)
                    return;
            }
        }

        public static void PrepareTestData()
        {
            var db = new ApplicationContext();

            using (db)
            {
                var userKate = new User { Name = "Kate", Surname = "Mask" };
                var userBob = new User { Name = "Bob", Surname = "Bezos" };
                var userAlice = new User { Name = "Alice", Surname = "Cooper" };
                var userBernar = new User { Name = "Bernar", Surname = "Arno" };

                if (db.Users != null)
                {
                    db.Users.AddRange(userKate, userBob, userAlice, userBernar);
                    db.SaveChanges();

                    var event0 = new Event
                    {
                        UserId = db.Users.FirstOrDefault(item =>
                            item.Name == userBob.Name && item.Surname == userBob.Surname).Id,
                        EventDate = Converter.StringToDateTime("2021-06-28 17:00 PM"),
                        EventType = EventTypeEnum.EnterTheRoom
                    };
                    var event1 = new Event
                    {
                        UserId = db.Users.FirstOrDefault(item =>
                            item.Name == userKate.Name && item.Surname == userKate.Surname).Id,
                        EventDate = Converter.StringToDateTime("2021-06-28 17:05 PM"),
                        EventType = EventTypeEnum.EnterTheRoom
                    };
                    var event2 = new Event
                    {
                        UserId = db.Users.FirstOrDefault(item =>
                            item.Name == userBob.Name && item.Surname == userBob.Surname).Id,
                        EventDate = Converter.StringToDateTime("2021-06-28 17:15 PM"),
                        EventType = EventTypeEnum.Comment, Value = "Hey, Kate - high five?"
                    };
                    var event3 = new Event
                    {
                        UserId = db.Users.FirstOrDefault(item =>
                            item.Name == userKate.Name && item.Surname == userKate.Surname).Id,
                        TargetUserId = db.Users.FirstOrDefault(item =>
                            item.Name == userBob.Name && item.Surname == userBob.Surname).Id,
                        EventDate = Converter.StringToDateTime("2021-06-28 17:17 PM"),
                        EventType = EventTypeEnum.HighFiveAnotherUser,
                        Value = MessageHelper.HighFives(userKate.Name, userBob.Name)
                    };
                    var event4 = new Event
                    {
                        UserId = db.Users.FirstOrDefault(item =>
                            item.Name == userBob.Name && item.Surname == userBob.Surname).Id,
                        EventDate = Converter.StringToDateTime("2021-06-28 17:18 PM"),
                        EventType = EventTypeEnum.LeaveTheRoom
                    };
                    var event5 = new Event
                    {
                        UserId = db.Users.FirstOrDefault(item =>
                            item.Name == userBob.Name && item.Surname == userBob.Surname).Id,
                        EventDate = Converter.StringToDateTime("2021-06-28 17:20 PM"),
                        EventType = EventTypeEnum.Comment, Value = "Oh, typical"
                    };
                    var event6 = new Event
                    {
                        UserId = db.Users.FirstOrDefault(item =>
                            item.Name == userBob.Name && item.Surname == userBob.Surname).Id,
                        EventDate = Converter.StringToDateTime("2021-06-28 17:21 PM"),
                        EventType = EventTypeEnum.LeaveTheRoom
                    };

                    var event20 = new Event
                    {
                        UserId = db.Users.FirstOrDefault(item =>
                            item.Name == userBob.Name && item.Surname == userBob.Surname).Id,
                        EventDate = Converter.StringToDateTime("2021-06-28 18:00 PM"),
                        EventType = EventTypeEnum.EnterTheRoom
                    };
                    var event21 = new Event
                    {
                        UserId = db.Users.FirstOrDefault(item =>
                            item.Name == userKate.Name && item.Surname == userKate.Surname).Id,
                        EventDate = Converter.StringToDateTime("2021-06-28 18:05 PM"),
                        EventType = EventTypeEnum.EnterTheRoom
                    };
                    var event22 = new Event
                    {
                        UserId = db.Users.FirstOrDefault(item =>
                            item.Name == userBob.Name && item.Surname == userBob.Surname).Id,
                        EventDate = Converter.StringToDateTime("2021-06-28 18:15 PM"),
                        EventType = EventTypeEnum.Comment, Value = "Hey, Kate - high five?"
                    };
                    var event23 = new Event
                    {
                        UserId = db.Users.FirstOrDefault(item =>
                            item.Name == userKate.Name && item.Surname == userKate.Surname).Id,
                        TargetUserId = db.Users.FirstOrDefault(item =>
                            item.Name == userBob.Name && item.Surname == userBob.Surname).Id,
                        EventDate = Converter.StringToDateTime("2021-06-28 18:17 PM"),
                        EventType = EventTypeEnum.HighFiveAnotherUser,
                        Value = MessageHelper.HighFives(userKate.Name, userBob.Name)
                    };
                    var event24 = new Event
                    {
                        UserId = db.Users.FirstOrDefault(item =>
                            item.Name == userBob.Name && item.Surname == userBob.Surname).Id,
                        EventDate = Converter.StringToDateTime("2021-06-28 18:18 PM"),
                        EventType = EventTypeEnum.LeaveTheRoom
                    };
                    var event25 = new Event
                    {
                        UserId = db.Users.FirstOrDefault(item =>
                            item.Name == userBob.Name && item.Surname == userBob.Surname).Id,
                        EventDate = Converter.StringToDateTime("2021-06-28 18:20 PM"),
                        EventType = EventTypeEnum.Comment, Value = "Oh, typical"
                    };
                    var event26 = new Event
                    {
                        UserId = db.Users.FirstOrDefault(item =>
                            item.Name == userBob.Name && item.Surname == userBob.Surname).Id,
                        EventDate = Converter.StringToDateTime("2021-06-28 18:21 PM"),
                        EventType = EventTypeEnum.LeaveTheRoom
                    };


                    db.Events.AddRange(event0, event1, event2, event3, event4, event5, event6,
                        event20, event21, event22, event23, event24, event25, event26);

                    db.SaveChanges();
                }
            }
        }
    }
}
