﻿using System;
using System.Collections.Generic;
using System.Linq;
using ChatHistoryViewer.Context;
using ChatHistoryViewer.Entities;
using ChatHistoryViewer.Enums;
using ChatHistoryViewer.Helper;
using ChatHistoryViewer.Repositories.Interfaces;
using Humanizer;

namespace ChatHistoryViewer.Repositories
{
    public class EventRepository : IEventRepository
    {
        private Dictionary<string, string> _eventsHistoryDict;
        private List<Tuple<string, string>> _aggregatedEvents;

        private ApplicationContext _context;
        public EventRepository()
        {
            _eventsHistoryDict = new Dictionary<string, string>();
            _aggregatedEvents = new List<Tuple<string, string>>();
            _context = new ApplicationContext();
        }

        public bool DeleteEvents()
        {
            try
            {
                if (_context == null) return false;

                foreach (var entity in _context.Events)
                    _context.Events.Remove(entity);
                _context.SaveChanges();

                return true;
            }
            catch
            {
                // ToDo: add exception logging and throw specified exception to the upper level

                return false;
            }
        }
        public bool AddEvent(Event entity)
        {
            try
            {
                if (_context == null) return false;

                _context.Events.Add(entity);
                _context.SaveChanges();

                return true;
            }
            catch(Exception)
            {
                // ToDo: add exception logging and throw specified exception to the upper level

                return false;
            }
        }

        public bool DeleteEventsByUser(string userName, string userSurname)
        {
            try
            {
                var user = _context?.Users.FirstOrDefault(item => item.Name == userName && item.Surname == userSurname);

                if (user == null) return false;

                if (_context.Events.Where(item=> item.UserId == user.Id) is { } eventsByUser)
                    foreach (var item in eventsByUser)
                    {
                        DeleteEvent(item);
                    }

                _context.SaveChanges();

                return true;
            }
            catch (Exception)
            {
                // ToDo: add exception logging and throw specified exception to the upper level

                return false;
            }
        }

        public bool DeleteEvent(Event entity)
        {
            try
            {
                if (_context == null) return false;

                _context.Events.Remove(entity);
                _context.SaveChanges();

                return true;
            }
            catch (Exception)
            {
                // ToDo: add exception logging and throw specified exception to the upper level

                return false;
            }
        }
        
        public Dictionary<string, string> GetDetailedEventsHistory(DateTime startTime, DateTime endTime)
        {
           if (_context == null) return null;

           var result = _context.Events.Where(e => e.EventDate > startTime && e.EventDate < endTime)
               .Join(_context.Users, e=>e.UserId, u=>u.Id,(e,u)=>new {e,u})
               .OrderBy(o => o.e.EventDate).ToList();

            _eventsHistoryDict = new Dictionary<string, string>();

            foreach (var item in result)
                AppendMessageByEventType(item.e, item.u.Name, null); //item.tu.Name

            return _eventsHistoryDict;
       }

       public List<Tuple<string, string>> GetAggregatedEventsHistory(DateTime startTime, DateTime endTime)
        {
            if (_context == null) return null;

            #region Data preparation

            var enumGeneralTypes = new List<EventTypeEnum>()
                   {EventTypeEnum.EnterTheRoom, EventTypeEnum.LeaveTheRoom, EventTypeEnum.Comment};

            var eventInfo = _context.Events
                   .Where(m => m.EventDate > startTime && m.EventDate < endTime && enumGeneralTypes.Contains(m.EventType))
                   .GroupBy(e => new { e.EventType, e.EventDate.Date, e.EventDate.Hour})
                   .OrderBy(e => e.Key.EventType)
                   .Select(o => Tuple.Create(o.Key, o.Count()));

            var highFiveSourceUsersInfo = _context.Events
               .Where(e => e.EventType == EventTypeEnum.HighFiveAnotherUser)
               .GroupBy(item => new { item.EventDate.Date, item.EventDate.Hour },
                            (key, elements) => new {
                                Date = key,
                                SourceCount = elements.Select(e => e.UserId).Distinct().Count(),
                            }
               );

            var highFiveTargetUsersInfo = _context.Events
               .Where(e => e.EventType == EventTypeEnum.HighFiveAnotherUser)
               .GroupBy(item => new { item.EventDate.Date, item.EventDate.Hour },
                   (key, elements) => new {
                       Date = key,
                       TargetCount = elements.Select(e => e.TargetUserId).Distinct().Count()
                   }
               );

            var hiFiveCommonInfo = highFiveSourceUsersInfo
               .Join(highFiveTargetUsersInfo, s => s.Date, t => t.Date, (s, t) => new { s, t })
               .Select(result => new
               {
                   result.s.Date,
                   result.s.SourceCount,
                   result.t.TargetCount
               });

            #endregion

            #region Data Populating

            _aggregatedEvents = new List<Tuple<string, string>>();

            foreach (var item in eventInfo)
            {
                AppendMessageByEventType(item.Item1.EventType,
                    $"{item.Item1.Date.ToShortDateString()} {item.Item1.Hour}",
                    item.Item2);
            }

            foreach (var item in hiFiveCommonInfo)
            {
                if (!_eventsHistoryDict.ContainsKey(item.Date.ToString()))
                    _aggregatedEvents.Add(new Tuple<string, string>(
                        $"{item.Date.Date.ToShortDateString()} {item.Date.Hour}",
                        MessageHelper.HighFives(item.SourceCount,
                            item.TargetCount)));
            }

            #endregion

            return _aggregatedEvents.OrderBy(item => item.Item1).ToList();
        }

       private void AppendMessageByEventType(EventTypeEnum eventType, string hour, int personsNumber)
       {
           try
           {
               switch (eventType)
               {
                   case EventTypeEnum.EnterTheRoom:
                       _aggregatedEvents.Add(new Tuple<string, string>(hour, MessageHelper.EnterTheRoom(personsNumber)));
                       break;
                   case EventTypeEnum.LeaveTheRoom:
                       _aggregatedEvents.Add(new Tuple<string, string>(hour, MessageHelper.LeaveTheRoom(personsNumber)));
                       break;
                   case EventTypeEnum.Comment:
                       _aggregatedEvents.Add(new Tuple<string, string>(hour, MessageHelper.LeftComments(personsNumber)));
                       break;
                   case EventTypeEnum.HighFiveAnotherUser:
                       break;
                   default:
                       throw new ArgumentOutOfRangeException();
               }
            }
           catch (ArgumentException ex)
           {
                Console.WriteLine(ex.Message, "skip adding item");
           }
             
       }

       private void AppendMessageByEventType(Event chatEvent, string nameFrom, string nameTo)
       {
           switch (chatEvent.EventType)
           {
               case EventTypeEnum.EnterTheRoom:
                   _eventsHistoryDict.Add(chatEvent.EventDate.ToShortTimeString(), MessageHelper.EnterTheRoom(nameFrom));
                   break;
               case EventTypeEnum.LeaveTheRoom:
                   _eventsHistoryDict.Add(chatEvent.EventDate.ToShortTimeString(), MessageHelper.LeaveTheRoom(nameFrom));
                   break;
               case EventTypeEnum.Comment:
                   _eventsHistoryDict.Add(chatEvent.EventDate.ToShortTimeString(), MessageHelper.LeftComments(nameFrom, chatEvent.Value));
                   break;
               case EventTypeEnum.HighFiveAnotherUser:
                   _eventsHistoryDict.Add(chatEvent.EventDate.ToShortTimeString(), MessageHelper.HighFives(nameFrom, nameTo));
                   break;
                default:
                   throw new ArgumentOutOfRangeException();
           }
       }
    }
}
