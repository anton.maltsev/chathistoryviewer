﻿using System;
using System.Collections.Generic;
using ChatHistoryViewer.Entities;

namespace ChatHistoryViewer.Repositories.Interfaces
{
    public interface IUserRepository
    {
        bool DeleteUsers();
        int? AddUser(User user);
        bool DeleteUser(User user);
        bool DeleteUserByNameSurname(string userName, string userSurname);
        List<User> GetUsers(List<int> usersIdsList);
        void UpdateUser(User currentUser);
    }
}
