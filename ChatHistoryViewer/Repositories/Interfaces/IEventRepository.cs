﻿using System;
using System.Collections.Generic;
using System.Text;
using ChatHistoryViewer.Context;
using ChatHistoryViewer.Entities;
using ChatHistoryViewer.Enums;
using Microsoft.EntityFrameworkCore.Infrastructure;

namespace ChatHistoryViewer.Repositories.Interfaces
{
    public interface IEventRepository
    {
        bool DeleteEvents();
        bool DeleteEventsByUser(string userName, string userSurname);
        bool AddEvent(Event entity);
        bool DeleteEvent(Event entity);
        List<Tuple<string, string>>  GetAggregatedEventsHistory(DateTime startTime, DateTime endTime);
        Dictionary<string, string> GetDetailedEventsHistory(DateTime startTime, DateTime endTime);
    }
}
