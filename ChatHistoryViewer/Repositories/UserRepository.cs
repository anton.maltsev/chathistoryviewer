﻿using System;
using System.Collections.Generic;
using System.Linq;
using ChatHistoryViewer.Context;
using ChatHistoryViewer.Entities;
using ChatHistoryViewer.Enums;
using ChatHistoryViewer.Helper;
using ChatHistoryViewer.Repositories.Interfaces;
using Microsoft.EntityFrameworkCore.Storage;

namespace ChatHistoryViewer.Repositories
{
    public class UserRepository : IUserRepository
    {
        private readonly ApplicationContext _context;
        public UserRepository()
        {
            _context = new ApplicationContext();
        }

        public bool DeleteUsers()
        {
            try
            {
                if (_context == null) return false;

                foreach (var user in _context.Users)
                    _context.Users.Remove(user);
                _context.SaveChanges();

                return true;
            }
            catch
            {
                // ToDo: add exception logging and throw specified exception to the upper level

                return false;
            }
        }

        public int? AddUser(User user)
        {
            try
            {
                if (_context == null) return null;

                _context.Users.Add(user);
                _context.SaveChanges();

                return _context.Users.FirstOrDefault(item=> item.Name == user.Name && item.Surname == user.Surname).Id;
            }
            catch (Exception ex)
            {
                // ToDo: add exception logging and throw specified exception to the upper level

                return null;
            }
        }

        public bool DeleteUser(User user)
        {
            try
            {
                if (_context == null) return false;

                _context.Users.Remove(user);
                _context.SaveChanges();

                return true;
            }
            catch (Exception ex)
            {
                // ToDo: add exception logging and throw specified exception to the upper level

                return false;
            }
        }

        public bool DeleteUserByNameSurname(string userName, string userSurname)
        {
            try
            {
                if (_context == null) return false;

                var user = _context.Users.FirstOrDefault(item => item.Name == userName && item.Surname == userSurname);

                if (user == null) return false;

                _context.Users.Remove(user);
                _context.SaveChanges();

                return true;
            }
            catch (Exception ex)
            {
                // ToDo: add exception logging and throw specified exception to the upper level

                return false;
            }
        }

        public List<User> GetUsers(List<int> usersIdsList)
        {
            return _context.Users.Where(u=> usersIdsList.Contains(u.Id)).ToList();
        }

        public void UpdateUser(User currentUser)
        {

            foreach (User user in _context.Users)
            {
                if (user.Id != currentUser.Id) continue;

                user.Name = currentUser.Name;
                user.Surname = currentUser.Surname;
                break;
            }

            _context.SaveChanges();
        }
    }
}
