using System;
using System.Diagnostics;
using ChatHistoryViewer.Entities;
using ChatHistoryViewer.Enums;
using ChatHistoryViewer.Helper;
using ChatHistoryViewer.Repositories;
using ChatHistoryViewer.Repositories.Interfaces;
using NUnit.Framework;
using SimpleInjector;
using ChatHistoryViewer;

namespace ChatHistoryViewer.Tests
{
    public class ChatHistoryTests
    {
        private Container _container;
        private IUserRepository _userRepository;
        private IEventRepository _eventRepository;
        private static DateTime _startDateTime;
        private static DateTime _endDateTime;

        [SetUp]
        public void Setup()
        {
            InitContainers();

            CleanTestData();
            Program.PrepareTestData();
        }

        private void InitContainers()
        {
            _container = IoCContainer.Default;
            _container.Options.AllowOverridingRegistrations = true;
            if(_userRepository == null)
                _container.Register<IUserRepository, UserRepository>();
            if (_eventRepository == null)
                _container.Register<IEventRepository, EventRepository>();

            _userRepository = _container.GetInstance<IUserRepository>();
            _eventRepository = _container.GetInstance<IEventRepository>();
        }

        [Test]
        public void AddUserPositiveTest()
        {
            var userTest = new User { Name = "Test", Surname = "Test" };

            var addResult = _userRepository.AddUser(userTest);
            Debug.Assert(addResult != null, nameof(addResult) + " != null");
            Assert.AreEqual(addResult.GetType(), typeof(int));

            var deleteResult = _userRepository.DeleteUser(userTest);
            Assert.AreEqual(deleteResult, true);
        }

        [Test]
        public void AddEventPositiveTest()
        {
            var userTest = new User { Name = "Test", Surname = "Test" };

            var addResult = _userRepository.AddUser(userTest);
            Assert.AreEqual(addResult.GetType(), typeof(int));

            var newEvent = new Event
            {
                UserId = (int) addResult, 
                EventDate = Converter.StringToDateTime("2021-06-28 17:00 PM"), 
                EventType = EventTypeEnum.EnterTheRoom
            };

            var addEventResult =_eventRepository.AddEvent(newEvent);
            Assert.AreEqual(addEventResult, true);

            var deleteUserResult = _userRepository.DeleteUser(userTest);
            Assert.AreEqual(deleteUserResult, true);

            var deleteEventResult = _eventRepository.DeleteEvent(newEvent);
            Assert.AreEqual(deleteEventResult, true);
        }

        [TestCase("06/01/2021", "08/01/2021")]
        public void EventsChronologicalOrderTest(string startDateTime, string endDateTime)
        {
            _startDateTime = Validator.DateTimeValidator(startDateTime);
            _endDateTime = Validator.DateTimeValidator(endDateTime);

            var events = _eventRepository.GetDetailedEventsHistory(_startDateTime, _endDateTime);
            DateTime? currentDateTime = null;
            var indicator = true;

            foreach (var e in events)
            {
                if (currentDateTime != null && Validator.DateTimeValidator(e.Key) <  currentDateTime)
                {
                    indicator = false;
                }

                currentDateTime = Validator.DateTimeValidator(e.Key);
            }

            Assert.AreEqual(indicator, true);
        }


        [TestCase(GranularityEnum.Hourly)]
        [TestCase(GranularityEnum.MinuteByMinute)]
        public void GranularityTypes(GranularityEnum granularityType)
        {
            Assert.AreEqual(granularityType.GetType(), typeof(GranularityEnum));
        }

        private void CleanTestData()
        {
            var userKate = new User { Name = "Kate", Surname = "Mask" };
            var userBob = new User { Name = "Bob", Surname = "Bezos" };
            var userAlice = new User { Name = "Alice", Surname = "Cooper" };
            var userBernar = new User { Name = "Bernar", Surname = "Arno" };

            _eventRepository.DeleteEventsByUser(userKate.Name, userKate.Surname);
            _eventRepository.DeleteEventsByUser(userBob.Name, userBob.Surname);
            _eventRepository.DeleteEventsByUser(userAlice.Name, userAlice.Surname);
            _eventRepository.DeleteEventsByUser(userBernar.Name, userBernar.Surname);

            _userRepository.DeleteUserByNameSurname(userKate.Name, userKate.Surname);
            _userRepository.DeleteUserByNameSurname(userBob.Name, userBob.Surname);
            _userRepository.DeleteUserByNameSurname(userAlice.Name, userAlice.Surname);
            _userRepository.DeleteUserByNameSurname(userBernar.Name, userBernar.Surname);
        }
    }
}